import json
import os


class SpikeSettings:

    def __init__(self):
        self.json_data = SpikeSettings.load_json()

    @staticmethod
    def load_json():
        filename = os.environ.get("SPIKE_SETTINGS_FILE", "settings/spike_settings.json")
        try:
            with open(filename, encoding="utf-8") as f:
                json_data = json.load(f)
            f.close()
        except FileNotFoundError:
            json_data = {}
        return json_data

    def save_settings(self):
        filename = os.environ.get("SPIKE_SETTINGS_FILE", "settings/spike_settings.json")
        with open(filename, 'w') as outfile:
            json.dump(self.json_data, outfile)
        outfile.close()

    def check_or_create_section(self, section):
        if self.json_data.get(section) is None:
            self.json_data[section] = {}

    @staticmethod
    def get_cyanide_key():
        json_data = SpikeSettings.load_json()
        return json_data.get("CYANIDE_KEY")

    @staticmethod
    def get_cyanide_bb2_base_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("CYANIDE_BB2_BASE_URL", "https://web1.cyanide-studio.com/ws/bb2/{}/")

    @staticmethod
    def is_sync_enabled():
        json_data = SpikeSettings.load_json()
        return json_data.get("SYNC_ENABLED", True)

    def set_sync_enabled(self, enable: bool = True):
        self.json_data["SYNC_ENABLED"] = enable
        self.save_settings()

    @staticmethod
    def get_discord_token():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_BOT", {}).get("TOKEN")

    @staticmethod
    def get_patreon_discord_token():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_BOT", {}).get("TOKEN_PATREON")

    @staticmethod
    def set_discord_token(self, token: str):
        self.check_or_create_section("SPIKE_BOT")
        self.json_data["TOKEN"] = token
        self.save_settings()

    @staticmethod
    def set_patreon_discord_token(self, token: str):
        self.check_or_create_section("SPIKE_BOT")
        self.json_data["TOKEN_PATREON"] = token
        self.save_settings()

    @staticmethod
    def get_nb_match_by_request():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_BOT", {}).get("MATCH_REPORT", {}).get("NB_MATCH_BY_REQUEST", 10)

    def set_nb_match_by_request(self, nb_match: int):
        self.json_data["SPIKE_BOT"]["MATCH_REPORT"]["NB_MATCH_BY_REQUEST"] = nb_match
        self.save_settings()

    @staticmethod
    def get_nb_league_by_request():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_BOT", {}).get("MATCH_REPORT", {}).get("NB_LEAGUE_BY_REQUEST", 10)

    def set_nb_league_by_request(self, nb_league: int):
        self.json_data["SPIKE_BOT"]["MATCH_REPORT"]["NB_LEAGUE_BY_REQUEST"] = nb_league
        self.save_settings()

    # Twitch
    @staticmethod
    def get_twitch_client_id():
        json_data = SpikeSettings.load_json()
        return json_data.get("TWITCH_BOT", {}).get("CLIENT_ID")

    @staticmethod
    def get_twitch_secret():
        json_data = SpikeSettings.load_json()
        return json_data.get("TWITCH_BOT", {}).get("SECRET")

    @staticmethod
    def get_twitch_token():
        json_data = SpikeSettings.load_json()
        return json_data.get("TWITCH_BOT", {}).get("TOKEN")

    @staticmethod
    def get_twitch_callback_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("TWITCH_BOT", {}).get("CALLBACK", "https://spike.ovh/twitch_callback")

    @staticmethod
    def get_twitch_channels():
        json_data = SpikeSettings.load_json()
        return json_data.get("TWITCH_BOT", {}).get("CHANNELS", ["poncho_dlv"])

    @staticmethod
    def add_twitch_channel(self, channel: str):
        json_data = SpikeSettings.load_json()
        channels = json_data.get("TWITCH_BOT", {}).get("CHANNELS", [])
        channels.append(channel)
        channels = set(channels)
        self.check_or_create_section("TWITCH_BOT")
        self.json_data["CHANNELS"] = list(channels)
        self.save_settings()

    @staticmethod
    def remove_twitch_channel(self, channel: str):
        json_data = SpikeSettings.load_json()
        channels = json_data.get("TWITCH_BOT", {}).get("CHANNELS", [])
        channels.remove(channel)
        self.check_or_create_section("TWITCH_BOT")
        self.json_data["CHANNELS"] = channels
        self.save_settings()

    # Steam
    @staticmethod
    def get_steam_api_key():
        json_data = SpikeSettings.load_json()
        return json_data.get("STEAM", {}).get("API_KEY")

    @staticmethod
    def get_steam_callback_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("STEAM", {}).get("CALLBACK")

    @staticmethod
    def get_steam_base_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("STEAM", {}).get("BASE_URL")

    # MongoDb Database
    @staticmethod
    def get_database_host():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_MONGO_DB", {}).get("HOST")

    @staticmethod
    def get_database_user():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_MONGO_DB", {}).get("USER")

    @staticmethod
    def get_database_password():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_MONGO_DB", {}).get("PASSWORD")

    # User Database
    @staticmethod
    def get_users_database_host():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_USERS_MONGO_DB", {}).get("HOST")

    @staticmethod
    def get_users_database_user():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_USERS_MONGO_DB", {}).get("USER")

    @staticmethod
    def get_users_database_password():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_USERS_MONGO_DB", {}).get("PASSWORD")

    # Spike Web
    @staticmethod
    def get_secret_key():
        json_data = SpikeSettings.load_json()
        return str(json_data.get("SECRET_KEY"))

    @staticmethod
    def get_sql_alchemy_database_uri():
        json_data = SpikeSettings.load_json()
        return json_data.get("SQLALCHEMY_DATABASE_URI")

    # discord OAuth2
    @staticmethod
    def get_discord_api_base_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("API_BASE_URL", "https://discordapp.com/api")

    @staticmethod
    def get_discord_authorization_base_url():
        json_data = SpikeSettings.load_json()
        return SpikeSettings.get_discord_api_base_url() + json_data.get("API_AUTHORIZE_URL", "/oauth2/authorize")

    @staticmethod
    def get_discord_token_url():
        json_data = SpikeSettings.load_json()
        return SpikeSettings.get_discord_api_base_url() + json_data.get("API_TOKEN_URL", "/oauth2/token")

    @staticmethod
    def get_discord_oauth2_client_secret():
        json_data = SpikeSettings.load_json()
        return json_data.get("OAUTH2_CLIENT_SECRET")

    @staticmethod
    def get_discord_oauth2_client_id():
        json_data = SpikeSettings.load_json()
        return json_data.get("OAUTH2_CLIENT_ID")

    @staticmethod
    def get_discord_oauth2_redirect_uri():
        json_data = SpikeSettings.load_json()
        return json_data.get("OAUTH2_REDIRECT_URI")

    # Spike Bot
    @staticmethod
    def get_big_leagues():
        json_data = SpikeSettings.load_json()
        return json_data.get("BIG_LEAGUES", [])

    @staticmethod
    def get_server_to_hide():
        json_data = SpikeSettings.load_json()
        ret = []
        servers = json_data.get("HIDE_SERVER", [])
        for server in servers:
            ret.append(server.get("id", 0))
        return ret

    @staticmethod
    def get_team_creator_key():
        json_data = SpikeSettings.load_json()
        return json_data.get("TC_KEY")

    @staticmethod
    def get_team_creator_base_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("TC_BASE_URL")

    @staticmethod
    def get_tournament_manager_key():
        json_data = SpikeSettings.load_json()
        return json_data.get("TM_KEY")

    @staticmethod
    def get_tournament_manager_base_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("TM_BASE_URL")

    @staticmethod
    def get_abuse_list():
        json_data = SpikeSettings.load_json()
        return json_data.get("ABUSE_LIST", [])

    @staticmethod
    def get_spike_api_key():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_API_KEY")

    @staticmethod
    def get_spike_api_base_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("SPIKE_API_BASE_URL")

    @staticmethod
    def get_idp_api_base_url():
        json_data = SpikeSettings.load_json()
        return json_data.get("IDP", {}).get("base_url")
