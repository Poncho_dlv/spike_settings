#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


class DiscordGuildSettings:

    def __init__(self, server_id: str):
        filename = f"settings/{server_id}.json"
        self.__server_id = server_id
        try:
            with open(filename, encoding="utf-8") as f:
                self.json_data = json.load(f)
            f.close()
        except FileNotFoundError:
            self.json_data = {}

    def save_settings(self):
        filename = f"settings/{self.__server_id}.json"
        with open(filename, 'w') as outfile:
            json.dump(self.json_data, outfile)
        outfile.close()

    def check_or_create_section(self, section):
        if self.json_data.get(section) is None:
            self.json_data[section] = {}

    def set_auto_role(self, role: int):
        self.check_or_create_section("admin")
        self.json_data["admin"]["auto_role"] = role
        self.save_settings()

    def get_auto_role(self) -> int:
        return self.json_data.get("admin", {}).get("auto_role", -1)

    def set_auto_role_enabled(self, role: bool):
        self.check_or_create_section("admin")
        self.json_data["admin"]["auto_role_enabled"] = role
        self.save_settings()

    def get_auto_role_enable(self) -> bool:
        return self.json_data.get("admin", {}).get("auto_role_enabled", False)

    def set_bot_admin_role(self, role: int):
        self.check_or_create_section("admin")
        self.json_data["admin"]["admin_role"] = role
        self.save_settings()

    def get_bot_admin_role(self) -> int:
        return self.json_data.get("admin", {}).get("admin_role", -1)

    # Main
    def set_server_name(self, name: str):
        self.check_or_create_section("main")
        self.json_data["main"]["server_name"] = name
        self.save_settings()

    def get_server_name(self) -> str:
        return self.json_data.get("main", {}).get("server_name")

    def set_platform(self, platform: str):
        self.check_or_create_section("main")
        self.json_data["main"]["platform"] = platform
        self.save_settings()

    def get_platform(self) -> str:
        return self.json_data.get("main", {}).get("platform", "pc")

    #  Local
    def set_timezone(self, timezone: str):
        self.check_or_create_section("local")
        self.json_data["local"]["timezone"] = timezone
        self.save_settings()

    def get_timezone(self) -> str:
        return self.json_data.get("local", {}).get("timezone", "UTC")

    def set_language(self, lang: str):
        self.check_or_create_section("local")
        self.json_data["local"]["lang"] = lang
        self.save_settings()

    def get_language(self) -> str:
        return self.json_data.get("local", {}).get("lang", "en")

    # Match report settings
    def set_compact_report(self, compact_report: bool):
        self.check_or_create_section("match_report")
        self.json_data["match_report"]["compact_report"] = compact_report
        self.save_settings()

    def get_compact_report(self) -> bool:
        return self.json_data.get("match_report", {}).get("compact_report", False)

    def set_display_impact_player(self, impact_player: bool):
        self.check_or_create_section("match_report")
        self.json_data["match_report"]["impact_player"] = impact_player
        self.save_settings()

    def get_display_impact_player(self) -> bool:
        return self.json_data.get("match_report", {}).get("impact_player", True)

    def set_enable_autoreg(self, enabled: bool):
        self.check_or_create_section("match_report")
        self.json_data["match_report"]["enable_autoreg"] = enabled
        self.save_settings()

    def get_enable_autoreg(self) -> bool:
        return self.json_data.get("match_report", {}).get("enable_autoreg", False)

    def set_autoreg_channel(self, channel: int):
        self.check_or_create_section("match_report")
        self.json_data["match_report"]["autoreg_channel"] = channel
        self.save_settings()

    def get_autoreg_channel(self) -> int:
        return self.json_data.get("match_report", {}).get("autoreg_channel", -1)

    # Welcome and leave settings
    def set_welcome_message_channel(self, channel: int):
        self.check_or_create_section("welcome_leave")
        self.json_data["welcome_leave"]["welcome_message_channel"] = channel
        self.save_settings()

    def get_welcome_message_channel(self) -> int:
        return self.json_data.get("welcome_leave", {}).get("welcome_message_channel", -1)

    def set_leave_message_channel(self, channel: int):
        self.check_or_create_section("welcome_leave")
        self.json_data["welcome_leave"]["leave_message_channel"] = channel
        self.save_settings()

    def get_leave_message_channel(self) -> int:
        return self.json_data.get("welcome_leave", {}).get("leave_message_channel", -1)

    def set_welcome_notification_role(self, role: int):
        self.check_or_create_section("welcome_leave")
        self.json_data["welcome_leave"]["welcome_notification_role"] = role
        self.save_settings()

    def get_welcome_notification_role(self) -> int:
        return self.json_data.get("welcome_leave", {}).get("welcome_notification_role", -1)

    def set_welcome_enabled(self, is_enabled: bool):
        self.check_or_create_section("welcome_leave")
        self.json_data["welcome_leave"]["enabled"] = is_enabled
        self.save_settings()

    def get_welcome_enabled(self) -> bool:
        return self.json_data.get("welcome_leave", {}).get("enabled", False)

    def set_welcome_message(self, message: str):
        self.check_or_create_section("welcome_leave")
        self.json_data["welcome_leave"]["welcome_message"] = message
        self.save_settings()

    def get_welcome_message(self) -> str:
        return self.json_data.get("welcome_leave", {}).get("welcome_message", "Welcome on {server} discord {user}!")

    def set_leave_message(self, message: str):
        self.check_or_create_section("welcome_leave")
        self.json_data["welcome_leave"]["leave_message"] = message
        self.save_settings()

    def get_leave_message(self) -> str:
        return self.json_data.get("welcome_leave", {}).get("leave_message", ":warning: {user} leave {server} :warning:")

    # Video settings
    def set_bb_channel(self, channel: int):
        self.check_or_create_section("vod")
        self.json_data["vod"]["bb_channel"] = channel
        self.save_settings()

    def get_bb_channel(self) -> int:
        return self.json_data.get("vod", {}).get("bb_channel", -1)

    def set_other_channel(self, channel: int):
        self.check_or_create_section("vod")
        self.json_data["vod"]["other_channel"] = channel
        self.save_settings()

    def get_other_channel(self) -> int:
        return self.json_data.get("vod", {}).get("other_channel", -1)

    def set_specific_channel(self, channel: int):
        self.check_or_create_section("vod")
        self.json_data["vod"]["specific_channel"] = channel
        self.save_settings()

    def get_specific_channel(self) -> int:
        return self.json_data.get("vod", {}).get("specific_channel", -1)

    def set_specific_channel_keywords(self, keywords: list):
        self.check_or_create_section("vod")
        self.json_data["vod"]["specific_channel_keywords"] = keywords
        self.save_settings()

    def get_specific_channel_keywords(self) -> list:
        return self.json_data.get("vod", {}).get("specific_channel_keywords", [])

    def set_live_channel(self, channel: int):
        self.check_or_create_section("vod")
        self.json_data["vod"]["live_channel"] = channel
        self.save_settings()

    def get_live_channel(self) -> int:
        return self.json_data.get("vod", {}).get("live_channel", -1)

    # Bounty
    def set_display_admin_bounty_only(self, state: bool):
        self.check_or_create_section("vod")
        self.json_data["vod"]["admin_only"] = state
        self.save_settings()

    def get_display_admin_bounty_only(self) -> bool:
        return self.json_data.get("bounty", {}).get("admin_only", False)

    def set_enable_bounty(self, state: bool):
        self.check_or_create_section("bounty")
        self.json_data["bounty"]["enabled"] = state
        self.save_settings()

    def get_enable_bounty(self) -> bool:
        return self.json_data.get("bounty", {}).get("enabled", True)
